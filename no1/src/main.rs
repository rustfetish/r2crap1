use ::cfonts::*;
use ::std::process::Command;
use ::yansi::*;

fn main() {
    clear_screen();
    println!("Hello, world!");
    colors();
}

// Defining colors here
fn colors() {
    let alert = Style::new(Color::Magenta).bold().italic().blink();
    let mis = "chams";
    println!("{}", alert.paint(mis));
}

fn clear_screen() {
    let mut c_cl = Command::new("clear");
    c_cl.status();
}